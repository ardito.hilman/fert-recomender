@extends('layout.base')
@section('content')

@include('Master.Component.dropzoneCard')
<div class="row">
  <div class="col-md-12">
    <!-- upload form -->
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">Import excel file</h3>
      </div>
      <div class="card-body">
      <form action="/import-dist-pupuk" method="post" enctype="multipart/form-data">
          <input type="file" name="file" required/>
          @csrf
          <button class="btn btn-success" type="submit">Submit</button>
      </form>
      </div>
      <div class="card-footer">
        Browse or drag file here 
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Distribusi Pupuk</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th rowspan="2">REGION NAME</th>
                            <th rowspan="2">COMPANY NAME</th>
                            <th rowspan="2">ESTATE NAME</th>
                            <th rowspan="2">KODE BA</th>
                            <th rowspan="2">JENIS PUPUPK</th>
                            <th class="text-center" colspan="12">DISTRIBUSI</th>
                        </tr>
                        <tr>
                            <th>JAN</th>
                            <th>FEB</th>
                            <th>MAR</th>
                            <th>APR</th>
                            <th>MEI</th>
                            <th>JUN</th>
                            <th>JUL</th>
                            <th>AUG</th>
                            <th>SEP</th>
                            <th>OCT</th>
                            <th>NOV</th>
                            <th>DES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data[0]))
                            @foreach($data[0] as $data)
                                <tr>
                                    <td>{{ $data[0] }}</td>
                                    <td>{{ $data[1] }}</td>
                                    <td>{{ $data[2] }}</td>
                                    <td>{{ $data[3] }}</td>
                                    <td>{{ $data[4] }}</td>
                                    <td>{{ $data[5] }}</td>
                                    <td>{{ $data[6] }}</td>
                                    <td>{{ $data[7] }}</td>
                                    <td>{{ $data[8] }}</td>
                                    <td>{{ $data[9] }}</td>
                                    <td>{{ $data[10] }}</td>
                                    <td>{{ $data[11] }}</td>
                                    <td>{{ $data[12] }}</td>
                                    <td>{{ $data[13] }}</td>
                                    <td>{{ $data[14] }}</td>
                                    <td>{{ $data[15] }}</td>
                                    <td>{{ $data[16] }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection

@include('Master.tableScript')