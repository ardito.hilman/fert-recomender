@extends('layout.base')
@section('content')

@include('Master.Component.dropzoneCard')
<div class="row">
  <div class="col-md-12">
    <!-- upload form -->
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">Import excel file</h3>
      </div>
      <div class="card-body">
      <form action="/test-api" method="post" enctype="multipart/form-data">
          <input type="file" name="file" required/>
          @csrf
          <button class="btn btn-success" type="submit">Submit</button>
      </form>
      </div>
      <div class="card-footer">
        Browse or drag file here 
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Hectare Statement</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>PERIODE BUDGET</th>
                            <th>BA CODE</th>
                            <th>AFD CODE</th>
                            <th>BLOCK CODE</th>
                            <th>HA PLANTED</th>
                            <th>TOPOGRAFI</th>
                            <th>LAND TYPE</th>
                            <th>JENIS BIBIT</th>
                            <th>LAND SUIT</th>
                            <th>TAHUN TANAM</th>
                            <th>JUMLAH POKOK</th>
                            <th>SPH</th>
                            <th>STATUS SMS1</th>
                            <th>STATUS SMS2</th>
                            <th>BLOCK DESC</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data[0]))
                            @foreach($data[0] as $data)
                                <tr>
                                    <td>{{ $data[0] }}</td>
                                    <td>{{ $data[1] }}</td>
                                    <td>{{ $data[2] }}</td>
                                    <td>{{ $data[3] }}</td>
                                    <td>{{ $data[4] }}</td>
                                    <td>{{ $data[5] }}</td>
                                    <td>{{ $data[6] }}</td>
                                    <td>{{ $data[7] }}</td>
                                    <td>{{ $data[8] }}</td>
                                    <td>{{ $data[9] }}</td>
                                    <td>{{ $data[10] }}</td>
                                    <td>{{ $data[11] }}</td>
                                    <td>{{ $data[12] }}</td>
                                    <td>{{ $data[13] }}</td>
                                    <td>{{ $data[14] }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection

@include('Master.tableScript')