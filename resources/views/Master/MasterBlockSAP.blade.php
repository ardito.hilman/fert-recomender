@extends('layout.base')
@section('content')

@include('Master.Component.dropzoneCard')
<div class="row">
  <div class="col-md-12">
    <!-- upload form -->
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">Import excel file</h3>
      </div>
      <div class="card-body">
      <form action="/import-blok-sap" method="post" enctype="multipart/form-data">
          <input type="file" name="file" required/>
          @csrf
          <button class="btn btn-success" type="submit">Submit</button>
      </form>
      </div>
      <div class="card-footer">
        Browse or drag file here 
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Master Block SAP</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>COM</th>
                            <th>COMPANY NAME</th>
                            <th>EST</th>
                            <th>AFD</th>
                            <th>BLOCK</th>
                            <th>BLOCK NAME</th>
                            <th>VALID FROM</th>
                            <th>VALID TO</th>
                            <th>BLOCK</th>
                            <th>REF BLOCK</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data[0]))
                            @foreach($data[0] as $data)
                                <tr>
                                    <td>{{ $data[0] }}</td>
                                    <td>{{ $data[1] }}</td>
                                    <td>{{ $data[2] }}</td>
                                    <td>{{ $data[3] }}</td>
                                    <td>{{ $data[4] }}</td>
                                    <td>{{ $data[5] }}</td>
                                    <td>{{ $data[6] }}</td>
                                    <td>{{ $data[7] }}</td>
                                    <td>{{ $data[8] }}</td>
                                    <td>{{ $data[9] }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection

@include('Master.tableScript')