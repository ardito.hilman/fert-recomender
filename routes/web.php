<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MasterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/distribution', function () {
    return view('Master.DistribusiPupuk');
});
Route::get('/lsu', function () {
    return view('Master.LeafSamplingUnit');
});
Route::get('/master-block-sap', function () {
    return view('Master.MasterBlockSAP');
});
Route::get('/hectar-statement', function () {
    return view('Master.MasterHS_BPS');
});
Route::get('/outlook-yield', function () {
    return view('Master.OutlookYield');
});
Route::get('/target-yield', function () {
    return view('Master.TargetYield3TH');
});

Route::group(['middleware'=> ['web']], function () {
    // display existed file to table
    Route::post('/import-hs', [MasterController::class, 'HsImport']);
    Route::post('/import-lsu', [MasterController::class, 'LsuImport']);
    Route::post('/import-blok-sap', [MasterController::class, 'BlokSapImport']);
    Route::post('/import-outlook-yield', [MasterController::class, 'OutlookYieldImport']);
    Route::post('/import-target-yield', [MasterController::class, 'TargetYieldImport']);
    Route::post('/import-dist-pupuk', [MasterController::class, 'DistPupukImport']);

    // upload file
    Route::post('/upload-file', [MasterController::class, 'Upload']);
    Route::post('/test-api', [MasterController::class, 'TestApi']);
});

// VUE SPA ROUTE
// Route::get('/{any}', function () {
//     return view('home');
// })->where('any', '.*');
