<?php

namespace App\Imports;

use App\ExcelModel;
use Maatwebsite\Excel\Concerns\ToModel;

class fileImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ExcelModel([
        ]);
    }
}
